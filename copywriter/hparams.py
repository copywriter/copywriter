from multiprocessing import cpu_count

vocab_size = 10000
embedding_dim = 300

encoder_dim = 4096
num_encoder = 4

hidden_sizes = (1000, 1000)

batch_size = 512
seq_len = 5

num_workers = 16
num_workers = min(num_workers, cpu_count())