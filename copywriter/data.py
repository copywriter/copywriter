import numpy as np

from magnet.data import Data, DIR_DATA

import torch
from torch.utils.data import Dataset

from newspaper import Article

DIR_DATA = (DIR_DATA / 'Copywriting').expanduser()

class NicheDataset(Dataset):
  def __init__(self, niche, nlp, encoder, n, embedding_size, num_workers=1):
    self.niche = niche

    urls = self.get_urls(DIR_DATA / niche / 'urls.csv')

    with torch.no_grad(): self.preprocess(urls, nlp, encoder, n, embedding_size, num_workers)

  def preprocess(self, urls, nlp, encoder, n, embedding_size, num_workers):
    from tqdm import tqdm_notebook as tqdm
    from functools import partial
    from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

    articles = [Article(url) for url in urls]

    def download(article, idx):
      if not (DIR_DATA / self.niche / (str(idx) + '.txt')).exists(): article.download()

    print('Downloading')
    with ThreadPoolExecutor(100) as executor:
      list(tqdm(executor.map(download, articles, range(len(articles))), total=len(articles)))

    print('Parsing')
    extract_text = partial(self.extract_text, n=n)
    with ProcessPoolExecutor(num_workers) as executor:
      list(tqdm(executor.map(extract_text, articles, range(len(articles))), total=len(articles)))

    print(f"Got {len(list((DIR_DATA / self.niche).glob('*.txt'))) / len(articles) * 100:.0f}% of the articles.")

    print('Encoding')
    extract_vectors = partial(self.extract_vectors, nlp=nlp, encoder=encoder, n=n, embedding_size=embedding_size)
    vectors = [vector for vector in tqdm(map(extract_vectors, range(len(articles))), total=len(articles)) if vector is not None]

    x, y = list(zip(*vectors))

    self.x = torch.cat(x)
    self.y = torch.cat(y)

  @staticmethod
  def get_urls(file):
    from csv import DictReader
    with open(file) as f: return [row['url'] for row in DictReader(f)]

  def extract_text(self, article, idx, n):
    try:
      path = DIR_DATA / self.niche
      path.mkdir(parents=True, exist_ok=True)
      path /= (str(idx) + '.txt')

      if path.exists(): return

      article.parse()
      text = article.text
      if text is None or text == '' or len(text) < n: return

      with open(path, 'w') as f: f.write(text)
    except: pass

  def extract_vectors(self, idx, nlp, encoder, n, embedding_size):
    path = DIR_DATA / self.niche / (str(idx) + '.txt')
    if not path.exists(): return None

    with open(path) as f: doc = nlp(f.read())
    if len(doc) < n: return None

    rows = len(doc) // n
    data = [doc[i:i + n] for i in range(0, len(doc) - n, n)]

    x = torch.tensor(encoder.encode([d.text for d in data])).float()
    y = torch.tensor([[NicheDataset.word_idx(token, nlp) for token in d] for d in data]).long()

    return x, y

  @staticmethod
  def word_idx(word, nlp):
    if isinstance(word, str): word = nlp(word)[0]
    idx = int(nlp.vocab.vectors.find(key=word.orth))
    if idx == -1: return nlp.vocab.vectors.shape[0]
    return idx

  def __getitem__(self, index):
    return self.x[index], self.y[index]

  def __len__(self):
    return len(self.x)

class NicheData(Data):
  def __init__(self, niche, nlp, encoder, n, embedding_size, val_split=0.2, **kwargs):
    path = DIR_DATA / niche
    super().__init__(path, **kwargs)

    num_workers = kwargs.pop('num_workers', 1)
    self._dataset = {'train': NicheDataset(niche, nlp, encoder, n, embedding_size, num_workers)}

    if val_split is not None: self._split_val(val_split)