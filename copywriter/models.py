import magnet as mag

import torch
import torch.nn.functional as F

from torch import nn

class Encoder(nn.Module):
  def __init__(self, vocab_size, embedding_dim, encoder_dim, num_layers):
    if encoder_dim % 4:
      raise ValueError(f"The encoder's dimensionality has to be"
                       "a multiple of 4. Got {encoder_dim}")

    super().__init__()

    self.embedding = nn.Embedding(vocab_size, embedding_dim)

    self.convs = nn.ModuleList([nn.Conv1d(embedding_dim, int(encoder_dim // 4), 3, 1, 1)])
    self.convs.extend([nn.Conv1d(int(encoder_dim // 4), int(encoder_dim // 4), 3, 1, 1)
                      for _ in range(num_layers - 1)])

  def forward(self, x):
    x = self.embedding(x).transpose(1, 2).contiguous()

    features = []
    for conv in self.convs:
      x = F.relu(conv(x))
      features.append(x.max(2)[0])

    return torch.cat(features, 1)

class Decoder(nn.Module):
  def __init__(self, encoder_dim, hidden_sizes, embedding_dim, output_len):
    super().__init__()

    self.fc = nn.Linear(encoder_dim, hidden_sizes[0] * output_len)
    self.convs = nn.ModuleList([nn.Conv1d(hidden_sizes[i], hidden_sizes[i + 1], 3, 1, 1)
                                for i in range(len(hidden_sizes) - 1)])
    self.convs.append(nn.Conv1d(hidden_sizes[-1], embedding_dim, 3, 1, 1))

  def forward(self, x):
    hidden_size = self.convs[0].in_channels
    x = F.relu(self.fc(x).view(x.shape[0], hidden_size, -1))

    for conv in self.convs[:-1]: x = F.relu(conv(x))
    return self.convs[-1](x)

def EncoderReadymade(vocab_size):
    from InferSent.models import InferSent
    from pathlib import Path
    V = 2
    DIR_MAIN = Path.cwd().parent
    MODEL_PATH = DIR_MAIN / ('InferSent/encoder/infersent%s.pkl' % V)
    params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': V}

    infersent = InferSent(params_model).to(mag.device)

    device = 'cuda:0' if mag.device == 'cuda' else 'cpu'
    infersent.load_state_dict(torch.load(MODEL_PATH, map_location=device))

    W2V_PATH = DIR_MAIN / 'InferSent/dataset/fastText/crawl-300d-2M.vec'
    infersent.set_w2v_path(W2V_PATH)

    infersent.build_vocab_k_words(K=vocab_size)

    return infersent.eval()