def get_nlp(model, vocab_size, save_path=None, prune_batch_size=8):
  import spacy

  nlp = spacy.load(model, disable=['parser', 'tagger', 'ner'])
  _set_vocab_size(nlp, vocab_size, prune_batch_size, save_path)
  return nlp

def _set_vocab_size(nlp, vocab_size, prune_batch_size, save_path=None):
  if save_path is not None and save_path.exists():
    nlp.vocab.from_disk(save_path)
    new_vocab_size = nlp.vocab.vectors.shape[0]
    if new_vocab_size != vocab_size: _prune_vocab(nlp, vocab_size, prune_batch_size, save_path)
  else: _prune_vocab(nlp, vocab_size, prune_batch_size, save_path)

def _prune_vocab(nlp, vocab_size, prune_batch_size, save_path=None):
  nlp.vocab.prune_vectors(vocab_size, prune_batch_size)
  if save_path is not None: nlp.vocab.to_disk(save_path)

def word_idx(word, nlp):
  if isinstance(word, str): word = nlp(word)[0]
  idx = int(nlp.vocab.vectors.find(key=word.orth))
  if idx == -1: return nlp.vocab.vectors.shape[0]
  return idx

def idx_word(idx, nlp):
  hash_code = nlp.vocab.vectors.find(row=idx)
  if len(hash_code) == 0: return '<UNK>'

  return nlp.vocab.strings[hash_code[0]]